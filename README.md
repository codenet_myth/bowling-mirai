### 1. Install dependencies



```bash
$ docker-compose build
$ docker-compose run --rm web npm install

```

### 2. Run project


```bash
$ docker-compose up

```

Project will run on http://localhost:8080